// add purple number through css

class MyHandler extends Paged.Handler {
    constructor(chunker, polisher, caller) {
        super(chunker, polisher, caller);
    }

    afterParsed(content) {
        content.querySelectorAll("p").forEach((element, i) => {
            element.setAttribute("data-num", i+1);
        })
    }
}

Paged.registerHandlers(MyHandler);
